import React, { useContext } from 'react'


const Error = () => {

    return (
        <div class="grid h-[80%] px-4 bg-[] place-content-center">
            <div class="text-center">
                <h1 class="font-black text-gray-400 text-9xl">404</h1>

                <p class="text-2xl font-bold tracking-tight text-gray-900 sm:text-4xl ">
                   Page Not Found!
                   
                </p>

                <p class="mt-4 text-gray-500">you're not allowed to access.</p>

                
            </div>
        </div>


    )
}

export default Error