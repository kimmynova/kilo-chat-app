import React from "react";
import Login from "../components/module/pages/login/Login";
import Register from "../components/module/pages/register/Register";
import GuestLayout from "../components/module/components/GuestLayout/GuestLayout";
import DefaultLayout from "../components/module/components/DefaultLayout/DefaultLayout";
import { createBrowserRouter } from "react-router-dom";
import Error from "./Error";
import ChatBody from "../components/module/pages/Chat/ChatBody/ChatBody";
import ChatGroup from "../components/module/pages/Chat/ChatGroup/ChatGroup";
import ChatContent from "../components/module/pages/Chat/ChatContent/ChatContent";
import Profile from "../components/module/pages/Profile/Profile";
import Setting from "../components/module/pages/Nav/Setting";

const routes = createBrowserRouter([
  {
    path: "/",
    element: <DefaultLayout />,
    children: [
      {
        path: "/",
        element: <ChatBody/>
      },
      {
        path: "chatgroup",
        element: <ChatGroup/>
      },
      {
        path:"/chat",
        element:<ChatContent/> ,
      },

      {
        path:"/profile",
        element:<Profile/>
      }
    ]
  },
  {
    path: "/",
    element: <GuestLayout />,
    children: [
      {
        path: "/login",
        element: <Login />,
        children: [],
      },
      {
        path: "/signup",
        element: <Register />,
      },
    ],
  },
  {
    path: "*",
    element: <Error />,
  },

]);

export default routes;
