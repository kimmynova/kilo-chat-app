import React from 'react'
import ReactDOM from 'react-dom/client';
import routers from './routes/routes';
import { RouterProvider } from 'react-router-dom';
import { ContextProvider } from './components/module/contexts/ContextProvider.jsx';
import { DarkmodePrvider } from './components/ui-theme/Darkmode.jsx';
import { AlertProvider } from './components/ui-theme/SweetAlert.jsx';

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
     <DarkmodePrvider>
      <AlertProvider>
    <ContextProvider>
        <RouterProvider router={routers} />
    </ContextProvider>
    </AlertProvider>
    </DarkmodePrvider>
  </React.StrictMode>,
)
