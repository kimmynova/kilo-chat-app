import React from 'react';

const Box = ({ children, className = '', ...props }) => {
  const baseStyles = 'border shadow-[3px_3px_16px_5px_#00000024]';
  
  return (
    <div className={`${baseStyles} ${className}`} {...props}>
      {children}
    </div>
  );
};

export default Box;
