import React, { forwardRef } from 'react';

const Input = forwardRef(({ className = '', ...props }, ref) => {
    // Define base styles with Tailwind CSS classes
    const baseStyles = "w-full px-5 py-3 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white dark:text-gray-500";

    return (
        <input
            ref={ref}
            className={`${baseStyles} ${className}`}
            {...props}
        />
    );
});

export default Input;
