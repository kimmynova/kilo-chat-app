import React from 'react';

const Image = ({ children, className = '', ...props }) => {
  const baseStyles = 'object-cover';
  
  return (
    <img alt="Photo Here 📸" className={`${baseStyles} ${className}`} {...props}>
      {children}
    </img>
  );
};

export default Image;
