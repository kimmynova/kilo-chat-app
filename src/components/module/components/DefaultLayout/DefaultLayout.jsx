import React, { useEffect } from 'react'
import Home from '../../pages/Home/Home'
import {  Navigate, Outlet } from 'react-router-dom'
import { useStateContext } from '../../contexts/ContextProvider'

const DefaultLayout = () => {
  const {  token } = useStateContext()
   
  if (!token) {
      return <Navigate to="/login" />
  }
  return (
    <div className='flex relative h-[100vh]  '>
        <Home/>
       <main className='w-full'>
          <Outlet />
       </main>
    </div>
 
  )
}

export default DefaultLayout