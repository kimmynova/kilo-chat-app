// GuestLayout.jsx
import React from 'react';
import { Outlet, Navigate } from 'react-router-dom';
import { useStateContext } from '../../contexts/ContextProvider';

export default function GuestLayout() {
    const { token } = useStateContext();
    if (token) {
        return <Navigate to="/" />;
    }
    return (
        <div className="flex justify-center items-center min-h-screen bg-white dark:bg-[#151521]">
            <div className="max-w-screen-xl w-full">
                <Outlet />
            </div>
        </div>
    );
}
