import React, { useState } from 'react'
import  '../ChatBody/chatBody.css'
import GroupItem from './groupList/GroupItem'
import ChatContent from  '../ChatContent/ChatContent'

const ChatGroup = () => {
  const [selectedChatUser, setSelectedChatUser] = useState({
    id: null,
    name: "",
    image: ""
  });
  const handleChatSelect = (userId) => {
    setSelectedChatUser(userId);
  };

  // handle back icon after return to chatItemList
  const handleBackClick = () => {
    setSelectedChatUser({
      id: null,
      name: "",
      image: "",
    });
  };
  return (
    <div className='main__chatbody dark:bg-[#151521ed]'>
      <GroupItem onGroupSelect={handleChatSelect}/>
      {selectedChatUser.id !== null && (
        <ChatContent selectedChatUser={selectedChatUser} onClickBack={handleBackClick} />
      )}
      {/* <ChatContent/> */}
    </div>
  )
}

export default ChatGroup