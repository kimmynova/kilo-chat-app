import { Add, Search } from "@mui/icons-material";
import Modal from "react-modal";
import React, { useState } from "react";
import ChatListItems from "../../ChatList/ChatListItems";
import logo from "../../../../../../assets/KiloIT-logo.png";
import "../../ChatBody/chatBody.css";

const GroupItem = ({ onGroupSelect }) => {
  const [allChats, setAllChats] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedMembers, setSelectedMembers] = useState([]);

  const members = [
    {
      id:1,
      name: "Momo",
      lastSeen: "39 minutes ago",
      img: "https://r.res.easebar.com/pic/20230213/7a6e4eda-e3fd-485e-b54e-62ba942ab95c.jpg",
    },
    {
      id:2,
      name: "Di zan",
      lastSeen: "8/27/2023",
      img: "https://www.pockettactics.com/wp-content/sites/pockettactics/2024/01/honor-of-kings-characters-supports.jpeg",
    },
    {
      id:3,
      name: "Di zan",
      lastSeen: "8/27/2023",
      img: "https://www.pockettactics.com/wp-content/sites/pockettactics/2024/01/honor-of-kings-characters-supports.jpeg",
    },
  ];

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  const handleSelectMember = (name) => {
    setSelectedMembers((prevSelected) =>
      prevSelected.includes(name)
        ? prevSelected.filter((member) => member !== name)
        : [...prevSelected, name]
    );
  };

  return (
    <div className="main__chatlist dark:text-[#fff]">
      <button
        className="dark:bg-[#151521] bg-[#3e85b0] p-3 w-[100%] "
        onClick={openModal}
      >
        <Add
          sx={{ color: "#fff", marginLeft: "0.2rem", alignItems: "start" }}
        />
        <span className="text-white">New Group</span>
      </button>
      <Modal
        isOpen={isModalOpen}
        onRequestClose={closeModal}
        contentLabel="New Conversation Modal"
        style={{
          overlay: {
            backgroundColor: "rgba(0, 0, 0, 0.5)",
          },
          content: {
            top: "50%",
            left: "50%",
            right: "auto",
            bottom: "auto",
            marginRight: "-50%",
            transform: "translate(-50%, -50%)",
            width: "400px",
            padding: "0px",
            border: "none"
          },
        }}
      >
        <div className="w-100 h-100 p-10 dark:bg-[#151521] dark:text-[#fff] ">
          <h2
            className="text-2xl  font-semibold mb-4"
            style={{ color: "#3E85B0" }}
          >
            Group Name
          </h2>
          <div>
            <input
              type="text"
              id="username"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="username"
              required
            />
          </div>
          <p className="mt-2 mb-2 font-medium">Members</p>
          <div className="member-list ">
            {members.map((member, index) => (
              <div className="member-item flex m-2" key={index}>
                <input
                  type="checkbox"
                  className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500"
                  checked={selectedMembers.includes(member.name)}
                  onChange={() => handleSelectMember(member.name)}
                />
                {member.img ? (
                  <img
                    src={member.img}
                    alt={member.name}
                    className="member-avatar"
                    style={{
                      objectFit: "cover",
                      width: "50px",
                      height: "50px",
                      borderRadius: "50%",
                    }}
                  />
                ) : (
                  <img
                    src="https://static-00.iconduck.com/assets.00/profile-circle-icon-512x512-zxne30hp.png"
                    alt={member.name}
                    className="member-avatar"
                    style={{
                      objectFit: "cover",
                      width: "50px",
                      height: "50px",
                      borderRadius: "50%",
                    }}
                  />
                )}
                <div className="member-info  ">
                  <div className="member-name dark:text-[#fff]">{member.name}</div>
                  <div className="member-last-seen">
                    last seen {member.lastSeen}
                  </div>
                </div>
              </div>
            ))}
          </div>

          <div className="flex justify-between w-full">
            <button
              type="submit"
              className="text-white mt-2 font-medium rounded-lg text-sm px-5 py-2.5 me-2 mb-2"
              style={{ backgroundColor: "#3E85B0" }}
            >
              Save
            </button>
            <button
              onClick={closeModal}
              type="button"
              className="text-white mt-2 font-medium rounded-lg text-sm px-5 py-2.5 me-2 mb-2"
              style={{ backgroundColor: "red" }}
            >
              Close
            </button>
          </div>
        </div>
      </Modal>
      <div className="chatList__search mt-2">
        <div className="search_wrap p-0">
          <input type="text" placeholder="Search Here" className="py-4 border-none  dark:text-[#151521] " required />
          <button className="search-btn dark:bg-[#151521] mb-0">
            <Search />
          </button>
        </div>
      </div>
      <div className="chatlist__items">
        <ChatListItems
          name="TKS"
          animationDelay="1"
          active="active"
          isOnline="active"
          image={logo}
          onClick={() => onGroupSelect({
            id: 1,
            name: "TKS",
            image: logo
          })}
        />
      </div>
    </div>
  );
};

export default GroupItem;
