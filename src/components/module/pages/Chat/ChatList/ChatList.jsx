import React, { Component } from "react";
import "./chat.css";
import ChatListItems from "./ChatListItems";
import { Add, Chat, Search } from "@mui/icons-material";
import Modal from "react-modal";

export default class ChatList extends Component {
  allChatUsers = [
    {
      image:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTA78Na63ws7B7EAWYgTr9BxhX_Z8oLa1nvOA&usqp=CAU",
      id: 1,
      name: "Tim Hover",
      active: true,
      isOnline: true,
    },
    {
      image:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTQEZrATmgHOi5ls0YCCQBTkocia_atSw0X-Q&usqp=CAU",
      id: 2,
      name: "Ayub Rossi",
      active: false,
      isOnline: false,
    },
  ];
  constructor(props) {
    super(props);
    this.state = {
      allChats: this.allChatUsers,
    };
  }

  handleChatSelect = (userId) =>{
    this.props.onChatSelect(userId);
  }

  openModal = () => {
    this.setState({ isModalOpen: true });
  };

  closeModal = () => {
    this.setState({ isModalOpen: false });
  };

  render() {
    return (
      <div className="main__chatlist sm:w-[100%] lg:w-[15%] dark:text-[#fff] ">
        <button
          className="dark:bg-[#151521] bg-[#3e85b0] p-3 w-[100%] "
          onClick={this.openModal}
        >
          <Add sx={{ color: "#fff", marginLeft: "0.2rem" }} />
          <span className="text-white ">New conversation</span>
        </button>
        <Modal
          isOpen={this.state.isModalOpen}
          onRequestClose={this.closeModal}
          contentLabel="New Conversation Modal"
          style={{
            overlay: {
              backgroundColor: "rgba(0, 0, 0, 0.5)",
            },
            content: {
              top: "50%",
              left: "50%",
              right: "auto",
              bottom: "auto",
              marginRight: "-50%",
              transform: "translate(-50%, -50%)",
              width: "400px",
              padding: "0px",
              border: "none"
            },
          }}
        >
          <div className="w-100 h-100 p-10 dark:bg-[#151521]">
            <h2
              className="text-2xl  font-semibold mb-4"
              style={{ color: "#3E85B0" }}
            >
              New Conversation
            </h2>
            <div>
              <input
                type="text"
                id="username"
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="username"
                required
              />
            </div>

            <div className="flex justify-between w-full">
              <button
                type="submit"
                class="text-white mt-2  font-medium rounded-lg text-sm px-5 py-2.5 me-2 mb-2 "
                style={{ backgroundColor: "#3E85B0" }}
              >
                Create User
              </button>
              <button
                onClick={this.closeModal}
                type="button"
                class="text-white mt-2  font-medium rounded-lg text-sm px-5 py-2.5 me-2 mb-2 "
                style={{ backgroundColor: "red" }}
              >
                Close
              </button>
            </div>
          </div>
        </Modal>

        <div className="chatlist__heading dark:bg-[#262631]">
          <h2 >Chats</h2>
          <button className="btn-nobg">
            <Chat />
          </button>
        </div>
        <div className="chatList__search">
          <div className="search_wrap p-0">
            <input
              type="text"
              placeholder="Search Here"
              className="py-4 dark:text-[#151521]  "
              required
            />
            <button className="search-btn dark:bg-[#151521]  mb-0 ">
              <Search />
            </button>
          </div>
        </div>
        <div className="chatlist__items">
          {this.state.allChats.map((item, index) => {
            return (
              <ChatListItems
                name={item.name}
                key={item.id}
                animationDelay={index + 1}
                active={item.active ? "active" : ""}
                isOnline={item.isOnline ? "active" : ""}
                image={item.image}
                onClick={() => this.handleChatSelect(item)}
              />
            );
          })}
        </div>
      </div>
    );
  }
}
