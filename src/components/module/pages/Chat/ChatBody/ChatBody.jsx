import React, { useState } from "react";
import ChatList from "../ChatList/ChatList";
import ChatContent from "../ChatContent/ChatContent";
import "./chatBody.css";

const ChatBody = () => {
  const [selectedChatUser, setSelectedChatUser] = useState({
    id: null,
    name: "",
    image: "",
  });
  const handleBackClick = () => {
    setSelectedChatUser({
      id: null,
      name: "",
      image: "",
    });
  };

  const handleChatSelect = (userId) => {
    setSelectedChatUser(userId);
  };

  return (
    <div className="main__chatbody  dark:bg-[#151521ed]">
      <ChatList onChatSelect={handleChatSelect} />
      {selectedChatUser.id !== null && (
        <ChatContent selectedChatUser={selectedChatUser} onClickBack={handleBackClick}  />
      )}

    </div>
  );
};
export default ChatBody;
