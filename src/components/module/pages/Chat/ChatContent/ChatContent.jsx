import React, { Component, createRef } from "react";
import Avatar from "../ChatList/Avatar";
import ChatItem from "./ChatItem";
import "./chatContent.css";
import {
  Add,
  AddPhotoAlternate,
  ArrowBack,
  Close,
  InsertDriveFile,
  Mic,
  Send,
  Stop,
} from "@mui/icons-material";
import { AudioRecorder } from "react-audio-voice-recorder";

export default class ChatContent extends Component {
  messagesEndRef = createRef(null);
  audioRecorderRef = createRef(null);

  chatItms = [
    {
      key: 1,
      userId: 1,
      image:
        "https://pbs.twimg.com/profile_images/1116431270697766912/-NfnQHvh_400x400.jpg",
      type: "",
      msg: "Hi Tim, How are you?",
    },
    {
      key: 2,
      userId: 1,
      image:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTA78Na63ws7B7EAWYgTr9BxhX_Z8oLa1nvOA&usqp=CAU",
      type: "other",
      msg: "I am fine.",
    },
    {
      key: 3,
      userId: 2,
      image:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTA78Na63ws7B7EAWYgTr9BxhX_Z8oLa1nvOA&usqp=CAU",
      type: "other",
      msg: "What about you?",
    },
  ];

  constructor(props) {
    super(props);
    this.state = {
      chat: this.chatItms,
      selectChatUser: null,
      msg: "",
      isRecording: false,
      recordingStartTime: null,
      recordingDuration: "00:00",
      dropdownOpen: false,
      selectedOption: "file", // 'file' or 'image'
    };
    this.recordingInterval = null;
  }
  scrollToBottom = () => {
    this.messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
  };

  componentDidMount() {
    window.addEventListener("keydown", (e) => {
      if (e.keyCode == 13) {
        if (this.state.msg != "") {
          this.chatItms.push({
            key: 1,
            type: "",
            msg: this.state.msg,
            image:
              "https://pbs.twimg.com/profile_images/1116431270697766912/-NfnQHvh_400x400.jpg",
          });
          this.setState({ chat: [...this.chatItms] });
          this.scrollToBottom();
          this.setState({ msg: "" });
        }
      }
    });
    this.scrollToBottom();
  }

  handleStateChange = (event) => {
    this.setState({ msg: event.target.value });
  };

  handleAudioData = (audioBlob) => {
    const audioUrl = URL.createObjectURL(audioBlob);
    this.chatItms.push({
      key: this.chatItms.length + 1,
      type: "",
      msg: (
        <audio controls>
          <source src={audioUrl} type="audio/wav" />
          Your browser does not support the audio element.
        </audio>
      ),
      image:
        "https://pbs.twimg.com/profile_images/1116431270697766912/-NfnQHvh_400x400.jpg",
    });
    this.setState({ chat: [...this.chatItms] });
    this.scrollToBottom();
  };

  handleSendMsg = () => {
    if (this.state.msg !== "") {
      this.chatItms.push({
        key: this.chatItms.length + 1,
        type: "",
        msg: this.state.msg,
        image:
          "https://pbs.twimg.com/profile_images/1116431270697766912/-NfnQHvh_400x400.jpg",
      });
      this.setState({ chat: [...this.chatItms], msg: "" });
      this.scrollToBottom();
    }
  };

  toggleAttachmentPopover = () => {
    this.setState((prevState) => ({
      showAttachmentPopover: !prevState.showAttachmentPopover,
    }));
  };

  selectAttachmentOption = (option) => {
    this.setState({
      selectedAttachmentOption: option,
      showAttachmentPopover: false,
    });
    console.log("Selected option:", option);
  };

  render() {
    const { showAttachmentPopover, selectedAttachmentOption } = this.state;
    const { selectedChatUser,onClickBack } = this.props;
    const filteredChat = this.chatItms.filter(
      (chat) => chat.userId === selectedChatUser.id
    );
    return (
      <div className="main__chatcontent md:w-3/4 lg:w-[85%] ">
        <div className="content__header dark:bg-[#151521]">
          <div className="blocks flex ">
            <span className="h-full pt-1 m-0">
            <ArrowBack
              className="cursor-pointer lg:hidden 2xl:hidden mr-3"
              onClick={onClickBack}
            />
            </span>
            <div className="current-chatting-user ">
              <Avatar isOnline="active" image={selectedChatUser.image} />
              <p>{selectedChatUser.name}</p>
            </div>
          </div>
        </div>

        <div className="content__body dark:bg-[#151521ed]">
          {filteredChat.map((itm, index) => (
            <ChatItem
              key={itm.key}
              user={itm.type ? itm.type : "me"}
              msg={itm.msg}
              image={itm.image}
            />
          ))}
          <div ref={this.messagesEndRef} />
        </div>

        <div className="content__footer dark:bg-[#151521ed] dark:text-[#fff]">
          <div className="sendNewMessage ">
            <button className="addFiles" onClick={this.toggleAttachmentPopover}>
              {showAttachmentPopover ? (
                <Close sx={{ marginBottom: "5px" }} />
              ) : (
                <Add sx={{ marginBottom: "5px" }} />
              )}
            </button>

            {showAttachmentPopover && (
              <div className="attachment-options">
                <button onClick={() => this.selectAttachmentOption("file")}>
                  <InsertDriveFile />
                </button>
                <button onClick={() => this.selectAttachmentOption("image")}>
                  <AddPhotoAlternate />
                </button>
              </div>
            )}

            {selectedAttachmentOption && (
              <p>Selected option: {selectedAttachmentOption}</p>
            )}
            <input
              type="text"
              placeholder="Type a message here"
              onChange={this.handleStateChange}
              value={this.state.msg}
              // className="dark:bg-[#fff]"
            />
            <button className="sendMsgBtn" onClick={this.handleSendMsg}>
              <Send sx={{ marginBottom: "5px" }} />
            </button>
            <div className="audioRecorder">
              <AudioRecorder
                ref={this.audioRecorderRef}
                onStop={this.handleAudioData}
                render={({ startRecording, stopRecording }) => (
                  <>
                    {!this.state.isRecording ? (
                      <button
                        onClick={() => {
                          this.setState({ isRecording: true });
                          startRecording();
                        }}
                      >
                        <Mic />
                      </button>
                    ) : (
                      <button
                        onClick={() => {
                          this.setState({ isRecording: false });
                          stopRecording();
                        }}
                      >
                        <Stop />
                      </button>
                    )}
                  </>
                )}
              />
              {this.state.isRecording && (
                <span className="recordingTimer">
                  {this.state.recordingDuration}
                </span>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
