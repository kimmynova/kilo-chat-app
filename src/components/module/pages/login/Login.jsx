import React, { createRef, useState } from 'react';
import Button from '../../../Layout/Button';
import Input from '../../../Layout/Input';
import { Link } from 'react-router-dom';
import Image from '../../../Layout/Image.jsx';
import Facebook from '../../../../assets/facebook.png';
import Google from '../../../../assets/google.png';
import Apple from '../../../../assets/apple-logo.png';
import KiloITLogo from "../../../../assets/KiloIT-logo.png"
import { useStateContext } from '../../contexts/ContextProvider.jsx';
import axiosClient from '../../contexts/axios-client.js';
const Login = () => {
  const usernameRef = createRef();
  const passwordRef = createRef();

  const { setUser, setToken } = useStateContext();
  const [errorMsg, setErrorMsg] = useState('');
  const [successMsg, setSuccessMsg] = useState('');
  // const [msg, setMsg] = useState('');

  const onSubmit = (e) => {
    e.preventDefault();
    const payload = {
      username: usernameRef.current.value,
      password: passwordRef.current.value,

    };
    axiosClient
      .post('auth/login', payload)
      .then((response) => {
        const { data } = response.data;
        const { user, token } = data;
        setUser(user);
        setToken(token);

        // localStorage.setItem('token', token);
      })
      .catch((error) => {
        if (error.response && error.response.status === 401 && error.response.data.message) {
          setErrorMsg(error.response.data.message);
        } else {
          setErrorMsg(error.response.data.message)
        }
      });

  };

  return (

    // <div className="h-[100vh] items-center flex justify-center px-5 lg:px-0 ">

    <div className="flex justify-center items-center h-screen bg-white dark:bg-[#151521]">
      <div className="max-w-screen-xl w-full">
        <div className="max-w-screen-xl round-xl bg-white border shadow sm:rounded-lg flex justify-center flex-1 dark:bg-[#151521]">
          <div className="flex-1 bg-blue-900 rounded-lg text-center hidden md:flex">
            <div
              className="m-12 xl:m-16 w-full bg-contain bg-center bg-no-repeat"
              style={{
                backgroundImage: `url(https://admin.kiloit.com/media/logos/final-logo.svg)`,
                // backgroundImage: `url(${KiloITLogo})`
              }}
            >
            </div>
          </div>
          <div className="flex-1 xl:w-5/12 p-6 sm:p-12 bg-white dark:bg-[#151521] dark:text-white" >
            <div className=" flex flex-col items-center">
              <div className="text-center">
                <h1 className="text-2xl xl:text-4xl font-extrabold text-blue-900 dark:text-white">
                  KiloChat
                </h1>
                <p className="text-[12px] text-gray-500 dark:text-white">
                  Hey enter your details your account
                </p>
              </div>
              {errorMsg && <div className="text-red-500">{errorMsg}</div>}
              {successMsg && <div className="text-green-500">{successMsg}</div>}
              <div className="w-full flex-1 mt-8">
                <div className="mx-auto max-w-xs flex flex-col gap-4">

                  <Input ref={usernameRef} type="text" placeholder="Email or Phone number" />

                  <Input ref={passwordRef} type="password" placeholder="Password" />

                  <Button onClick={onSubmit}>
                    <span className="ml-3">Sign In</span>
                  </Button>
                  <p className='text-center my-2'>Or continus connect with</p>
                  <div className='flex items-center justify-center gap-6'>
                    <Image src={Facebook} style={{ width: '35px', height: '35px',cursor:`pointer` }} />
                    <Image src={Google} style={{ width: '35px', height: '35px',cursor:`pointer` }} />
                    {/* <Image src={Apple} style={{ width: '35px', height: '35px' }}/> */}
                  </div>
                  <p className="mt-6 text-xs text-gray-600 text-center dark:text-white">
                    Not Yet have an account?{" "}
                    <Link to={"/signup"}>
                      <span className="text-blue-900 font-semibold dark:text-white">Sign up</span>
                    </Link>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;
