import React, { createRef, useState } from 'react';
import Button from '../../../Layout/Button'
import Input from '../../../Layout/Input';
import { Link } from 'react-router-dom';
import { useStateContext } from '../../contexts/ContextProvider';
import axiosClient from '../../contexts/axios-client';
const Login = () => {

  const usernameRef = createRef();
  const nameRef = createRef();
  const emailRef = createRef();
  const phoneRef = createRef();
  const passwordRef = createRef();
  const passwordConfirmationRef = createRef();
  const { setUser, setToken } = useStateContext()
  const [errorMsg, setErrorMsg] = useState('');
  const [successMsg, setSuccessMsg] = useState('');

  const onSubmit = (e) => {
    e.preventDefault();

    const PayLoad = {
      // username:usernameRef.current.value,
      name: nameRef.current.value,
      email: emailRef.current.value,
      phone: phoneRef.current.value,
      password: passwordRef.current.value,
      confirmPassword: passwordConfirmationRef.current.value,
    }

    // console.log(PayLoad);
    axiosClient.post('/auth/register', PayLoad)
      .then((response) => {
        const data = response.data
        setUser(data.user)
        setToken(data.token)
      })
      .catch((error) => {
        if (error.response && error.response.status === 401 && error.response.data.message) {
          setErrorMsg(error.response.data.message);
        } else {
          setErrorMsg(error.response.data.message)
        }

      })
  }



  return (

    // <div className="h-[100vh] items-center flex justify-center px-5 lg:px-0 ">
    <div className="flex justify-center items-center h-screen">
      <div className="max-w-screen-xl w-full">
        <div className="max-w-screen-xl round-xl  bg-white dark:bg-[#151521] dark:text-white border shadow sm:rounded-lg flex justify-center flex-1">
          <div className="flex-1 rounded-lg bg-blue-900 text-center hidden md:flex">
            <div
              className="m-12 xl:m-16 w-full bg-contain bg-center bg-no-repeat "
              style={{
                backgroundImage: `url(https://admin.kiloit.com/media/logos/final-logo.svg)`,
              }}
            >
            </div>
          </div>
          <div className="lg:w-1/2 flex-1 xl:w-5/12 p-6 sm:p-12">
            <div className=" flex flex-col items-center">
              <div className="text-center">
                <h1 className="text-2xl xl:text-4xl font-extrabold text-blue-900 dark:text-white">
                  KiloChat
                </h1>
                <p className="text-[12px] text-gray-500 dark:text-white">
                  Hey enter your details to create your account
                </p>
              </div>
              {errorMsg && <div className="text-red-500">{errorMsg}</div>}
              {successMsg && <div className="text-green-500">{successMsg}</div>}
              <div className="w-full flex-1 mt-8">
                <div className="mx-auto max-w-xs flex flex-col gap-4">

                  <Input ref={nameRef} type="text" placeholder="Full Name" />

                  {/* <Input  ref={usernameRef} type="text" placeholder="Name" /> */}

                  <Input ref={emailRef} type="email" placeholder="Email" />

                  <Input ref={passwordRef} type="password" placeholder="Password" />

                  <Input ref={passwordConfirmationRef} type="password" placeholder="Comfirm password" />

                  <Input ref={phoneRef} type="number" placeholder="Phone" />

                  <Button onClick={onSubmit}>
                    <span className="ml-3">Sign Up</span>
                  </Button>
                  <p className="mt-6 text-xs text-gray-600 text-center dark:text-white">
                    Already have an account?{" "}
                    <Link to={"/Login"}>
                      <span className="text-blue-900 font-semibold dark:text-white">Sign In</span>
                    </Link>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;




