import { AccountCircle, Logout, Phone} from '@mui/icons-material';
import CloseIcon from '@mui/icons-material/Close';
import KeyboardReturnIcon from '@mui/icons-material/KeyboardReturn';
import React, { useContext } from 'react';
import { AlertContext } from '../../../ui-theme/SweetAlert';
import Cookies from "js-cookie";

const ProfilePopup = () => {
    const AlertBox = useContext(AlertContext);
  
    const handleLogout = async() => {
        const dbox = await AlertBox.logoutmsg();
        if (dbox.isConfirmed) {
            // localStorage.removeItem('token');
            Cookies.remove("secret-key")
            setTimeout(() => {
                   window.location.reload()
            }, 1000);
          
        }
      }

  return (
    // <div class="w-[] bg-gray-100 dark:bg-[#999999]">
    <div class="relative max-w-sm mx-auto ">
        <div class="rounded overflow-hidden shadow-md bg-white dark:bg-[#151521] ">
            < CloseIcon className='dark:text-white'/>
            
            <div class="absolute -mt-20 w-full flex justify-center">
                <div class="h-32 w-32">
                    <img src="https://randomuser.me/api/portraits/women/49.jpg" class="mt-2 rounded-full object-cover h-full w-full shadow-md" />
                </div>
            </div>
            <div class="px-6 mt-16 dark:bg-[#151521] dark:text-white">
                <h1 class="font-bold text-2xl text-center mb-1">Jes Cyka</h1>
                <p class="text-gray-800 text-sm text-center dark:text-white">Chief Executive Officer</p>
                {/* <p class="text-center text-gray-600 text-base pt-3 font-normal">
                    Hello my name is Heng Sokly
                </p> */}

                <div class="max-w-md space-y-1 mt-6">
                    <div class="flex items-center p-3">
                        <AccountCircle/>
                        <p class="ml-2">Username:</p>
                        <p class="text-blue-600 ml-2">@jescyka168</p>
                    </div>
                    <div class="flex items-center p-3">
                        <Phone/>
                        <p class="ml-2">Phone Number:</p>
                        <p class="text-blue-600 ml-2">0987654321</p>
                    </div>
                </div>

                <div class="w-full flex justify-center pt-5 pb-5">
                    {/* <button>
                        <Logout/>
                    </button> */}
                    <button  type="button" class="text-white bg-red-700 hover:bg-red-800  font-medium rounded-full text-sm px-5 py-2.5 text-center me-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700" onClick={handleLogout}>
                        Logout
                    </button>
                </div>
            </div>
        </div>
    </div>
// </div>   
  );    
};

export default ProfilePopup;
