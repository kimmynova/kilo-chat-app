import React, { useState } from "react";
import "./nav.css";
import Darkmode from '../Nav/Darkmode';

import { Link } from "react-router-dom";
import { Close, Logout, Menu, PowerOff, PowerSettingsNewOutlined, Token } from "@mui/icons-material";
import { SideBarRoute } from "./SideBarRoute";
import logo from "../../../../assets/KiloIT-logo.png";
import Setting from "./Setting";
import Popup from "reactjs-popup";


const Nav = () => {
  const [sidebar, setSidebar] = useState(false);
  const showSidebar = () => setSidebar(!sidebar);

  const handleLogout = () => {
    localStorage.removeItem('token');
    setTimeout(() => {
      window.location.reload();
    }, 800);

  }
  const [isDarkMode, setIsDarkMode] = useState(false);

  const toggleDarkMode = () => {
    setIsDarkMode(!isDarkMode);
  };



  const [showSettings, setShowSettings] = useState(false); // State to manage visibility of settings
  const toggleSettings = () => {
    setShowSettings(!showSettings);
  };
  return (
    <div>
      <div className="navbar dark:bg-[#151521] px-6 py-2.5 overflow-hidden">
        <Link to="#" className="menu-bars">
          <Menu onClick={showSidebar} />
        </Link>

      </div>
      <nav className={sidebar ? "nav-menu active" : "nav-menu"} onClick={showSidebar}>
        <div className="sidebar-menu ">
          <ul
            className="nav-menu-items dark:bg-[#151521]"
          >
            <div className="image-profile">
              <div>
                <img
                  src={logo}
                  alt=""
                  width={100}
                  height={100}
                  style={{
                    objectFit: "cover",
                    borderRadius: "50%",
                    border: "1px solid rgba(0, 0, 0, 0.091)",
                  }}
                />
                <h3 className="font-semibold pt-2 pb-2">HONG HONG</h3>
              </div>
              <li className="navbar-toggle dark:bg-[#151521]">
                <Link to="#" className="menu-bars dark:bg-[#151521] ">
                  <Close sx={{ width: 30, height: 30, float: "right" }} onClick={showSidebar} />
                </Link>
              </li>
            </div>

            <ul>
              {SideBarRoute.map((item, index) => (
                <li key={index} className={item.cName}>
                  {item.path ? (
                    <Link to={item.path}>
                      {item.icon}
                      <span>{item.title}</span>
                    </Link>
                  ) : (
                    <>
                      {item.title === 'Setting' ? (
                        <div className="nav-text cursor-pointer hover:bg-[#262631] transition: all 0.2s ease-in">
                          {item.icon}
                          <Popup
                            trigger={<li className="nav-text w-[234px] ">{item.title}</li>}
                            position="center center"
                            modal
                            closeOnDocumentClick
                            onOpen={toggleSettings}
                            onClose={toggleSettings}
                          >
                            <Setting />
                          </Popup>
                        </div>
                      ) : (
                        <>
                          <div class="nighticon hover:bg-[#262631] transition: all 0.2s ease-in">
                            {item.icon}
                            <span>{item.title}</span>
                          </div>
                          {item.title === 'Night Mode' && (
                            <Darkmode isDarkMode={isDarkMode} toggleDarkMode={toggleDarkMode} />
                          )}

                        </>
                      )}
                    </>
                  )}
                </li>
              ))}
            </ul>

          </ul>
        </div>
      </nav>
    </div>
  );
};
export default Nav;
