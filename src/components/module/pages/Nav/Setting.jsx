import React, { useState } from 'react';
import Image from '../../../Layout/Image';
import profile from '../../../../assets/KiloIT-logo.png';
import { PermIdentity as PermIdentityIcon } from '@mui/icons-material';
import Popup from 'reactjs-popup';
import ProfilePopup from '../Profile/Profile';

const Setting = () => {
    const [showSettingForm, setShowSettingForm] = useState(true);
    const [showProfilePopup, setShowProfilePopup] = useState(false);

    const toggleSettingForm = () => {
        setShowSettingForm(!showSettingForm);
    };

    const toggleProfilePopup = () => {
        setShowProfilePopup(!showProfilePopup);
    };

    return (
        <>
            {showSettingForm && (
                <div className={`transition duration-300 dark:bg-black dark:text-white m-auto w-72 h-[200px] flex flex-col gap-2 bg-neutral-50 rounded-lg shadow p-2 ${showProfilePopup ? 'hidden' : ''}`}>
                    <div className="flex gap-2">
                        {/* <Image src={profile} className="w-24 h-24 shrink-0 rounded-full" alt="" /> */}
                        <div className="flex flex-col">
                            <span className="font-bold text-neutral-700 dark:text-white italic">Setting</span>
                            {/* <p className="line-clamp-3">Kilo-IT</p>
                            <p className="line-clamp-3">0965991403</p>
                            <p className="line-clamp-3">@Kilo-IT</p> */}
                        </div>
                    </div>
                    <div className="flex flex-col bg-neutral-50 border list-none dark:bg-slate-400">
                        <li
                            className="cursor-pointer flex items-center hover:bg-gray-600 hover:text-white text-[22px] gap-2"
                            onClick={toggleProfilePopup}
                        >
                            <PermIdentityIcon size={80} /> Account Setting
                        </li>
                    </div>
                </div>
            )}

            {showProfilePopup && (
                <div className="max-w-sm mx-auto mt-[-80%]">
                    <ProfilePopup />
                </div>
            )}
  </>
    )}
export default Setting
