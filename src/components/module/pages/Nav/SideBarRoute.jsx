import { Chat, Group, Person, PortableWifiOff, Settings } from '@mui/icons-material';
import DarkModeOutlinedIcon from '@mui/icons-material/DarkModeOutlined';

import React, { useState } from 'react'

export const SideBarRoute = [

  {
    title: 'Chat',
    path: '/',
    icon: <Chat />,
    cName: 'nav-text'
  },
  {
    title: 'Chat Group',
    path: '/chatgroup',
    icon: <Group />,
    cName: 'nav-text'
  },
  {
    title: 'Setting',
    icon: <Settings />,
    cName: 'nav-text'
  },
  {
    title: 'Night Mode',
    icon: <DarkModeOutlinedIcon />,
    cName: 'nav-text',
  },

];

