import React from "react";
import Nav from "../../pages/Nav/Nav.jsx";
import "./home.css";

const Home = () => {
  return (
    <div className="__main">
      <Nav />
    </div>
  );
};

export default Home;
