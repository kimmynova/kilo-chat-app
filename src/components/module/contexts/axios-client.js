
import axios from 'axios';
import { useNavigate } from 'react-router';

const axiosClient= axios.create({
    headers:{
      Authorization: "Basic ZWNvbTplY29tQDEyMw==",
    },
    // baseURL: `${process.env.REACT_APP_API_BASE_URL}/api/v1`
     baseURL: `${import.meta.env.VITE_API_BASE_URL}/api/v1`
});
//?request
axiosClient.interceptors.request.use((config) => {
  const token = localStorage.getItem('token');
  if (token) {
      config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

axiosClient.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      const { response } = error;

      // const path =useNavigate()

      if (response && response.status === 401) {
        localStorage.removeItem('ACCESS_TOKEN');
          return Promise.reject(error);
      }

      throw error;
    }
  );


    //!interceptors is use for excute when request is senting or after repon is recive

export default axiosClient;
