import { createContext, useContext, useState } from "react";
import Cookies from "js-cookie";
const StateContext = createContext({
    currentUser: null,
    token: null,
    notification:null,
    setUser: () => {},
    setToken: () => {},
    setNotification:()=>{},
});

export const ContextProvider = ({ children }) => {
    const [user, setUser] = useState({});
    const [notification,_setNofitication] = useState('');
    // const [token, _setToken] = useState(localStorage.getItem('token'));
    const [token, _setToken] = useState(Cookies.get('secret-key'));
    
    const setToken = (token) => {
        // console.log('Setting token:', token);
        _setToken(token);
        if (token) {
            // localStorage.setItem('token', token);
            Cookies.set('secret-key', token, { expires: 7, secure: true, sameSite: 'Strict' });
        } else {
            // localStorage.removeItem('token');
            Cookies.remove('secret-key');
        }
    };
const setNotification=(Message)=>{
    _setNofitication(Message)
   setTimeout=(()=>{
    _setNofitication("")
   },5000);
}
    return (
        <StateContext.Provider value={{
            user,
            token,
            setUser,
            setToken,
            notification,
            setNotification,

        }}>
            {children}
        </StateContext.Provider>
    );
};
export const useStateContext = () => useContext(StateContext);